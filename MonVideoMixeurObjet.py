import re
from tkinter import *
from tkinter import messagebox
from PIL import Image, ImageTk
#from tkinter.ttk import *
##import ffmpeg
import tkinter.filedialog
import os,sys
import subprocess as sp
import threading

def crop(rognage):
        dimension = rognage.split('x')
        dim = [int(val) for val in dimension]
        x1, y1 = str(dim[0]), str(dim[1])
        dx, dy = str(dim[2]-dim[0]), str(dim[3]-dim[1])
        return f"-filter:v crop={dx}:{dy}:{x1}:{y1}"
    

class MonApplication(Frame):
    """ Défini la 'lobjet application avec les propriétés suivantes :
    
    master = objet tkinter parent
    larg = largeur de la fenêtre
    haut = hauteur de la fenêtre
    
    source        = str - chemin complet du fichier source a convertir
    chemin_source = str - chemin vers le dossier du fichier source
    nom_source    = str - nom du fichier source
    chemin_destination = str - chemin vers le destination du fichier destination
    nom_destination = str - nom du fichier destination
    destination   = str - ancienne version chemin complet destination
    liste_programmes = list - liste contenant une liste des différents les programmes dans le fichier.
                        Chaque éléement de la liste est une liste [compteur, [programme], [video], [audio], [sous-titre]]
    programme = list -  [numéro de p dans le flux, nom du service]
    liste_audio = list - contient les  différentes pistes audio
    liste_soustitre = list - contient les différentes pistes de sous titre
    rognage = list  - contient la liste des coordonnées de la partie de la vidéo à garder [x1,y1,x2,y2) 
    codec = str - contient le codec pour la conversion
    taille = str - contient la taille du initiale de la vidéo : largeur x hauteur
    liste_segments = list -  liste de tuples (début, fin) des parties à découper et à garder
    largeur_video = str - largeur de la vidéo initiale
    hauteur_video = str - hauteur de la vidéo initiale
    """
    def __init__(self, larg, haut, master=Tk(), nom='Mon vidéo mixeur'):
        Frame.__init__(self, master)
        self.config(width = larg, height = haut)
        self.master = master
        self.larg = larg
        self.haut = haut
        self.grid()
        #
        # varriables du projet point de vue application
        #
        self.source= ''
        self.chemin_source = ''
        self.chemin_destination = ''
        self.nom_source=''
        self.nom_destination = ''
        self.destination = ""
        self.tmp = ''
        self.liste_programmes = []
        self.programme = ''
        self.liste_audio = []
        self.liste_soustitre = []
        self.rognage = []
        self.codec ='libx264'
        self.taille = '720x576'
        self.liste_segments = []
        self.largeur_video=''
        self.hauteur_video=''
        self.duration = ''
        self.start = ''
        
        
        self.nom_prog=nom
        
        
        #
        # barre de menu
        #
        self.menu_barre=Menu(self.master)
        self.master.config(menu = self.menu_barre)
        self.menu_fichier=Menu(self.menu_barre)
        self.menu_fichier.add_command(label='Nouveau', accelerator="Ctrl + N",underline =0, command =self.nouveau)
        self.menu_fichier.add_command(label='Ouvrir', accelerator="Ctrl + O",underline =0, command =self.ouvrir)
        self.menu_fichier.add_command(label='Fermer', accelerator="Alt + F",underline =0, command =self.fermer)
        self.menu_fichier.add_separator()
        self.menu_fichier.add_command(label='Enregistrer', accelerator="Ctrl + S", command =self.sauver)
        self.menu_fichier.add_command(label='Enregistrer sous', accelerator="Ctrl+Shift+ S", command =self.sauver_sous)
        self.menu_fichier.add_separator()
        self.menu_fichier.add_command(label='Quitter', accelerator="Alt+F4", command =self.quitter)
        self.menu_barre.add_cascade(label='Fichier',menu=self.menu_fichier,underline=0)
        #
        self.menu_vue=Menu(self.menu_barre, tearoff=0)
        self.menu_vue.add_command(label='Nouveau', accelerator="Ctrl + N",underline =0,command =self.nouveau)
        self.menu_vue.add_command(label='Ouvrir', accelerator="Ctrl + O",underline =0, command =self.ouvrir)
        self.menu_barre.add_cascade(label='Vue',menu=self.menu_vue,underline=0)
        #
        self.menu_aide=Menu(self.menu_barre, tearoff=0)
        self.menu_aide.add_command(label='À propos',underline =0, command =self.apropos)
        self.menu_aide.add_command(label='Aide', accelerator="F1",underline =0, command =self.aide)
        self.menu_barre.add_cascade(label='Aide',menu=self.menu_aide,underline=0)
        
        #
        # Barre d'icône
        #
        self.barre_icone=Frame(self,height=50)
        self.icon_nouveau=PhotoImage(file='./icons/icon_nouveau.png')
        Button(self.barre_icone,image=self.icon_nouveau,command=self.nouveau).grid(row=1, column=1)
        self.icon_ouvrir=PhotoImage(file='./icons/icon_ouvrir.png')
        Button(self.barre_icone,image=self.icon_ouvrir,command=self.ouvrir).grid(row=1, column=2)
        self.icon_sauver=PhotoImage(file='./icons/icon_sauver.png')
        Button(self.barre_icone,image=self.icon_sauver,command=self.sauver).grid(row=1, column=6)
        self.icon_valider=PhotoImage(file='./images/icone_ok.png')
        Button(self.barre_icone,image=self.icon_valider,command=self.valider).grid(row=1, column=8)
        self.icon_aide=PhotoImage(file='./icons/icon_aide.png')
        Button(self.barre_icone,image=self.icon_aide,command=self.aide).grid(row=1, column=10)
        Button(self.barre_icone).grid(row=1, column=3,columnspan=3)
        Button(self.barre_icone).grid(row=1, column=7)
        self.barre_icone.grid(row=1,column=1, columnspan=10)

        #
        # Frame principale : résumé
        #
        
        # fichier source et destination
        self.frame_fichier=LabelFrame(self, text='Fichiers')
        Button(self.frame_fichier,text='Selection Source', command=self.nouveau).grid(row=2, column=2)
        self.source_fichier=Label(self.frame_fichier, text='pas de fichier',width=30, anchor="e")
        self.source_fichier.grid(row=2, column=3,columnspan=2, padx=10,ipadx=10)
        Button(self.frame_fichier,text='Selection Destination', command=self.exporter).grid(row=2, column=6)
        self.destination_fichier=Label(self.frame_fichier, text='pas de fichier',width=30)
#         self.fin = StringVar()
#         Entry(self, textvariable= self.fin, width = 13).grid(row=1, column=7)
        self.destination_fichier.grid(row=2, column=7,columnspan=2, padx=10,ipadx=10)
        self.frame_fichier.grid(row=2,column=1)
        
        # programme choisi
        self.frame_programme=LabelFrame(self, text='Film',width=0)
        Button(self.frame_programme,text='Selection programme', command=self.selection_programme).grid(row=1, column=1)
        self.programme_label=Label(self.frame_programme, text='choisir un programme', width=20)
        self.programme_label.grid(row=1, column=3,columnspan=2, padx=10,ipadx=10)
        self.frame_programme.grid(row=3,column=1)
        # liste audios et sous titre
        self.piste=Frame(self.frame_programme)
        Label(self.piste,text='Pistes choisies : ').grid(row=1, column=1)
        Button(self.piste,text='Selection audio', command=self.selection_audio).grid(row=1, column=2)
        Button(self.piste,text='Selection sous-titre', command=self.selection_soustitre).grid(row=1, column=6)
        self.audio_label=LabelFrame(self.piste, text='Audio',height=3)
        self.audio_label.grid(row=2, column=2,columnspan=2, padx=10,ipadx=10)
        self.audio_list = Message(self.audio_label,
                                  width=10)
        self.set_audio()
        self.audio_list.grid(row = 4, column =2)

        self.soustitre_label=LabelFrame(self.piste, text='Sous-titre')
        self.soustitre_label.grid(row=2, column=6,columnspan=2, padx=10,ipadx=10)
        self.soustitre_list=Message(self.soustitre_label,
                                  width=20)
        self.set_soustitre()
        self.soustitre_list.grid(row=4, column = 6)
        self.piste.grid(row=4,column=1,padx=5, pady=5)

       # liste propriétés
        self.caracteristique=LabelFrame(self,text='Propriétés')
        self.caracteristique.grid(row=1, column=2,padx=5,pady=5)
        self.codec_label=Label(self.caracteristique, text='codec : h264')
        self.codec_label.grid(row=2, column=2,columnspan=1, padx=10,ipadx=10)
        Button(self.caracteristique,text='Codec', command=self.selection_codec).grid(row=3, column=2)

        self.rognage_label=Label(self.caracteristique, text='')
        self.rognage_label.grid(row=2, column=3,columnspan=2, padx=10,ipadx=10)
        Button(self.caracteristique,text='Rognage', command=self.selection_rognage).grid(row=3, column=3)

        self.decoupage_label=Label(self.caracteristique, text='')
        self.decoupage_label.grid(row=4, column=5,columnspan=2, padx=10,ipadx=10)
        Button(self.caracteristique,text='decoupage', command=self.selection_decoupage).grid(row=3, column=5)

        self.taille_initiale_label=Label(self.caracteristique, text='Taille initiale : ')
        self.taille_initiale_label.grid(row=2, column=5,columnspan=2, padx=10,ipadx=10)

        self.taille_finale_label=Label(self.caracteristique, text='Taille finale : ')
        self.taille_finale_label.grid(row=2, column=7,columnspan=2, padx=10,ipadx=10)
        self.caracteristique.grid(row=6,column=1)
        
        
    #
    # fonctions
    #
    def init_projet(self, src = ''):
        self.set_source(src)
        self.set_destination(self.source.split(SEPARATEUR)[-1].split('.')[0]+".mkv")
        self.liste_programmes = []
        self.set_programme ()
        self.liste_audio = []
        self.set_audio()
        self.liste_soustitre = []
        self.set_soustitre()
        self.rognage = []
        self.set_rognage()
        self.codec ='libx264'
        self.set_codec()
        self.taille = ''
        self.liste_segments = []
        self.largeur_video=''
        self.hauteur_video=''
        self.duration = ''
        self.start = ''

        
    def set_source(self,src=''):
        self.source=src
        if self.source != '':
            self.chemin_source = os.path.dirname(self.source)
            self.nom_source = os.path.basename(self.source)
            self.source_fichier.configure(text=src)
            self.tmp = self.chemin_source + SEPARATEUR + 'tmp_' + self.nom_source.split('.')[0]
            self.master.title('{} - {}'.format("Mon mixeur vidéo",os.path.abspath(self.source)))
        else :
            self.source_fichier.configure(text='Choisir un fichier source')
            self.master.title('{} '.format("Mon mixeur vidéo"))

    def set_destination(self,dest=''):
        self.destination = dest
        if self.destination != '':
            self.chemin_destination = os.path.dirname(self.destination)
            self.nom_destination = os.path.basename(self.destination)
            self.tmp = self.chemin_destination + SEPARATEUR + 'tmp_' + self.nom_destination.split('.')[0]
            self.destination_fichier.configure(text= self.destination)
        else :
            self.master.title('{} '.format("Mon mixeur vidéo"))

    def nouveau(self):
        if self.selection_source():
            self.init_projet(src = self.source)
            #self.analyse_source()
           # self.set_programme()
           # if len(self.liste_programmes) == 1 :
            #    self.set_programme(1)# = self.liste_programmes[0][0]
            #    self.programme_label.configure(text=self.liste_programmes[0][1][2])
        else:
            self.set_source(self.source)
            self.set_destination(self.destination)
            self.set_programme(self.programme)
            
    def exporter(self):
        if self.selection_destination():
            self.set_destination(self.destination)
        else:
            self.set_destination()
    # sélectionne le fichier source
    def selection_source(self):
        nbProgram=0
        self.source =tkinter.filedialog.askopenfilename(defaultextension=".mkv",filetypes=[("Ts vidéos","*.ts"),("All Files", "*.*"), ("Mkv vidéos","*.mkv") ])
        
        if self.source:
            self.destination_fichier.configure(text= self.source.split(SEPARATEUR)[-1].split('.')[0]+".mkv")
            self.programme_label.configure(text='Choisir un programme')
            return True
        else :
            self.programme_label.configure(text='Choisir une source')
            return False
    # sélectionne le fichier destination
    def selection_destination(self):
        fichier =tkinter.filedialog.asksaveasfilename(defaultextension=".mkv", initialfile=self.nom_destination.split('.')[0],filetypes=[("All Files", "*.*"), ("Mkv vidéos","*.mkv") ])
        if fichier:
            self.chemin_destination = os.path.dirname(fichier)
            self.nom_destination = os.path.basename(fichier)
            extension = self.nom_destination.split('.')[1]
            if  extension == 'mkv':
                self.destination_fichier.configure(text= self.destination)
                self.destination = fichier
            else:
                self.destination_fichier.configure(text='Choisir une autre destination1')
        return self.destination
        
    # analyse le fichier source avec ffprobe pour creer un fichier "result_ffprobe_tmp.txt"
    # qui décrit le contenu du fichier :  différents programmes vidéos, avec leur piste audio et sous titre
    # lancer au moment du choix du programme

    def analyse_source(self): 
        """ lance ffprobe et redirige la sortie vers le fichier texte
            ffprobe_nom_source_tmp.txt
        
        """
        if not os.path.exists(self.tmp + SEPARATEUR):
            os.mkdir(self.tmp + SEPARATEUR)
        Source_ffprobe=self.tmp + SEPARATEUR + "/result_"+self.nom_source.split('.')[0] +"_tmp.txt"
        if not os.path.exists(Source_ffprobe):
            command = [ FFPROBE_BIN,'-i', self.source]
            fichier = open(Source_ffprobe, "w") # Ouvre le fichier.
            output = sp.Popen(command, stdout=sp.PIPE, stderr=sp.STDOUT,universal_newlines=True).communicate()[0]
            fichier.write(output)
            fichier.close()
        ## lance l'analyse de la sortie ffprobe, pour compter le nombre de programme avec piste vidéos
        fichier = open(Source_ffprobe, "r") # Ouvre le fichier.
        lignes_texte = fichier.readlines()
        fichier.close()
        ## Cherche le nombre de programme, si 0 mettre à 1 : Il existe mais ne possède pas l'intitulé programme
        numProgram = 0
        numero1=0
        liste_programmes = []
        for n in range(1,len(lignes_texte)-1):
            if "Duration" in lignes_texte[n]:
                regexpDuration = " *Duration: +(?P<duration>[0-9:.]+), *start: +(?P<start>[0-9:.]+),"
                result = re.match(regexpDuration, lignes_texte[n])
                if result:
                    self.duration = result.group('duration')
                    self.start = result.group('start')
            if "Program" in lignes_texte[n]:
                if ("Program" in  lignes_texte[n+1]) != True :
                    regexpProgram = " *Program +(?P<numProg>[0-9]+)"
                    result=re.match(regexpProgram, lignes_texte[n])
                    if result:
                        numProgram+=1
                        liste_programmes.append([numProgram,['Programme',result.group('numProg')]])
                    if numero1 == 0:
                        numero1=1
        if numProgram==0 :
            numProgram=1
            liste_programmes.append([numProgram,['Programme','undefined','unique']])
  
        # complète la liste des programmes avec les renseignements de chacun des programmes

        numProgram=0
        for n in range(numero1,len(lignes_texte)):
            if "Program" in lignes_texte[n]:
                    if (n != len(lignes_texte)-1) and ("Program" in  lignes_texte[n+1]) != True :
                        regexpProgram = " *Program +(?P<numProg>[0-9]+)"
                        result=re.match(regexpProgram, lignes_texte[n])
                        if result:
                            numProgram+=1
            regexpServiceName = " *service_name *: *(?P<serviceName>[ \?\-\w]*)"
            result=re.match(regexpServiceName, lignes_texte[n])
            if result:
                liste_programmes[numProgram-1][1].append(result.group('serviceName'))
            regexpStreamV = " *Stream #0:(?P<numStream>[0-9]+).*Video.+, (?P<largeur>[0-9]+)x(?P<longueur>[0-9]+).+"
            result=re.match(regexpStreamV, lignes_texte[n])
            if result:
                liste_programmes[numProgram-1].append(['Video',result.group('numStream'),result.group('largeur'),result.group('longueur')])
            regexpStreamA = " *Stream #0:(?P<numStream>[0-9]+)(\[.+\])?(?P<lang>\([a-w]+\))?: Audio.+?(?P<impaired>\([a-w]+ impaired\))?$"
            result=re.match(regexpStreamA, lignes_texte[n])
            if result:
                var_impaired = str(result.group('impaired'))
                if var_impaired == str(None):
                    var_impaired = ''
                liste_programmes[numProgram-1].append(['Audio',[result.group('numStream'),result.group('lang')+var_impaired]])
            regexpStreamS = r" *Stream #0:(?P<numStream>[0-9]+)(\[.+\])?(?P<lang>\([a-w]+\))?: Subtitle.+?(?P<impaired>\([a-w]+ impaired\))?$"
            result=re.match(regexpStreamS, lignes_texte[n])
            if result:
                var_impaired = str(result.group('impaired'))
                if var_impaired == str(None):
                    var_impaired = ''
                liste_programmes[numProgram-1].append(['Sous-titre',[result.group('numStream'),result.group('lang')+var_impaired]])
        lliste_prog=[]
        numProgram=0
        for n in range(0,len(liste_programmes)):
            if len(liste_programmes[n])>2:
                numProgram += 1
                lliste_prog.append(liste_programmes[n])
                lliste_prog[-1][0] = numProgram
        if numProgram==1 :
            lliste_prog[0][1].append('unique')
        self.liste_programmes = lliste_prog
    
    def set_programme(self,prog=0):
        self.programme=prog
        if self.programme != 0:
            self.programme_label.configure(text=self.liste_programmes[prog-1][1][2])
            self.largeur_video = self.liste_programmes[prog-1][2][2]
            self.hauteur_video = self.liste_programmes[prog-1][2][3]
            self.taille_initiale_label.configure(text='Taille initiale : '+ self.largeur_video+'x'+self.hauteur_video)
            self.rognage = 'x'.join(['0',
                                   '0',
                                   self.largeur_video,
                                   self.hauteur_video])
        else :
            self.programme_label.configure(text='Choisir un programme')

    def selection_programme(self):
        if self.source :
            self.analyse_source()
            Programmes(self)
        else:
            print('couucouc',self.source)
            messagebox.showinfo("Alerte", "veuillez choisir un fichier source")
            self.nouveau()

# selection audio
    def set_audio(self):
        liste=[]
        for i in range(len(self.liste_audio)):
            liste.append('- '+self.liste_audio[i][1]+'\n')
        while len(liste)<3:
            liste.append('- \n')
        self.audio_list.configure(text='\n'.join(liste),width=0)

    def selection_audio(self):
        Audio(self)

# selection soustitre
    def set_soustitre(self):
        liste=[]
        for i in range(len(self.liste_soustitre)):
            liste.append('- '+self.liste_soustitre[i][1]+'\n')
        while len(liste)<3:
            liste.append('- \n')
        self.soustitre_list.configure(text='\n'.join(liste),width=0)

    def selection_soustitre(self):
        Soustitre(self)

# selection codec
    def set_codec(self,codec = 'libx264'):
        self.codec=codec
        self.codec_label.configure(text='codec : '+codec)

    def selection_codec(self):
        self.set_codec('h265')

# selection rognage
    def set_rognage(self, rognage='non défini'):
        self.rognage=rognage
        self.rognage_label.configure(text='rognage : '+self.rognage)
        dim=rognage.split('x')
        if rognage != 'non défini':
            self.taille_finale_label.configure(text='Taille finale : '+ str(int(dim[2])-int(dim[0]))+'x'+str(int(dim[3])-int(dim[1])))
        else:
            self.taille_finale_label.configure(text='Taille finale : non définie')

    def selection_rognage(self):
        if not os.path.exists(self.tmp +
                              SEPARATEUR ):
            os.mkdir(self.tmp +
                     SEPARATEUR)
        if self.liste_segments:
            temps_initial = (20*60+float(self.liste_segments[0][0]))
        else:
            temps_initial = (float(20*60))
        if not os.path.exists(self.tmp + SEPARATEUR + 'image001.PNG'):
            command = [ FFMPEG_BIN]
            fichier = open(self.tmp + SEPARATEUR + "mycommande.txt", "a")
        
            fichier.write('commande de rognage \n\n')
            fichier.write('\n nom de la source : ' + self.source)
            fichier.write('\n nom de la destination : ' + self.destination)
            fichier.write('\n programme : ' + self.liste_programmes[self.programme-1][1][2])
            fichier.write('\n largeur initiale : ' + self.largeur_video)
            fichier.write('\n hauteur initiale : ' +  self.hauteur_video)
            for i in range(len(self.liste_segments)):
                fichier.write('\n liste segments temps : ' + str(self.liste_segments), sep=' ')
            fichier.write('\nDossier temporaire : ' + self.tmp)
            fichier.write('\n timestamp initial : ' + self.start)
            for i in range(5):
                temps_initial += 300
                command = command + ['-ss', str(temps_initial), '-i', self.source,
                            '-frames', '1',
                            '-map', '0:'+self.liste_programmes[self.programme-1][2][1],
                            '-f', 'image2', self.tmp + SEPARATEUR + 'image00'+str(i+1)+'.PNG']
            
            fichier.write('\n commande : '  + str(' '.join(command)))
            fichier.write('\n\n\n')
            fichier.close()
            output = sp.Popen(command).communicate()[0]
        self.rogne = Rognage(self)
        
# selection decoupage horaire
    def set_decoupage(self,decoupage='non défini'):
        self.decoupage=decoupage
        self.decoupage_label.configure(text='decoupage : '+ str(decoupage))

    def selection_decoupage(self):
        self.set_decoupage(self.liste_segments)
        Decoupage(self)

    def ouvrir(self):
        print('ouvrir')

    def sauver(self):
        print('sauver')
        
    def sauver_sous(self):
        print('sauver sous')
        
    def quitter(self):
        exit()
        
    def fermer(self):
        print('source : ' , self.source)
        print('chemin de la source :',self.chemin_source)
        print('nom de la source :',self.nom_source)
        print('destination : ', self.destination)
        print('chemin de la destination :',self.chemin_destination)
        print('nom de la destination :',self.nom_destination)
        print(' liste des programme : ', self.liste_programmes[self.programme-1][1][2])
        print(' largeur initiale : ',self.largeur_video)
        print(' hauteur initiale : ', self.hauteur_video)
        print(' pistes audio : ', self.liste_audio)
        print(' pistes sous titres : ', self.liste_soustitre)
        print( ' rognage : ', self.rognage)
        print(' liste segments temps : ', self.liste_segments)
        print('Dossier temporaire : ', self.tmp)
        print('Durée source : ', self.duration)
        print('Start source : ', self.start)
        
    def valider(self):
        #
        # conversion des différentes parties
        #
        macommand = [FFMPEG_BIN, '-y', '-max_error_rate', '1', '-v', 'verbose',
                         '-max_interleave_delta', '2000000',  '-i', self.source,
                         '-threads', '4', '-preset',  'slow', '-crf', '21', '-tune', 'film',
                         '-profile:v', 'high10']
        rognage = crop(self.rognage)
        video = ['-map',  '0:' + self.liste_programmes[self.programme-1][2][1], '-vcodec', 'libx264']
        audio = []
        for piste in self.liste_audio:
            audio += ['-map', '0:' + piste[0]]
        audio += ['-acodec', 'eac3']
        soustitre = []
        for piste in self.liste_soustitre:
            soustitre += ['-map', '0:' + piste[0]]
        soustitre += ['-scodec', 'copy']
        for i in range(len(self.liste_segments)):
            macommand += ['-ss', self.liste_segments[i][0]] + rognage.split(' ')
            macommand += ['-to', self.liste_segments[i][1]] + video + audio+ soustitre
            macommand += [self.tmp + SEPARATEUR + 'partie_' + str(i) + '.mkv']
        print('au travail, conversion')
        fichier = open(self.tmp + SEPARATEUR + "mycommande.txt", "a")
        
        fichier.write('résumé final puis commande de validation \n\n')
        fichier.write('\n nom de la source : ' + self.source)
        fichier.write('\n nom de la destination : ' + self.destination)
        fichier.write('\n programme : ' + self.liste_programmes[self.programme-1][1][2])
        fichier.write('\n largeur initiale : ' + self.largeur_video)
        fichier.write('\n hauteur initiale : ' +  self.hauteur_video)
        fichier.write('\n pistes audio : ' + str(self.liste_audio))
        fichier.write('\n pistes sous titres : ' + str(self.liste_soustitre))
        fichier.write('\n rognage : ' + self.rognage)
        for i in range(len(self.liste_segments)):
            fichier.write('\n liste segments temps : ' + str(self.liste_segments))
        fichier.write('\nDossier temporaire : ' + self.tmp)
        fichier.write('\n timestamp initial : ' + self.start)
        fichier.write('\n commande : '  + str(' '.join(macommand)))
        fichier.write('\n\n\n')
        fichier.close()
        output = sp.Popen(macommand).communicate()[0]
        #
        # Création du répertoire destination temporaire
        #
        if not os.path.exists(self.tmp +
                              SEPARATEUR +
                              self.nom_destination.split('.')[0] +
                              SEPARATEUR):
            os.mkdir(self.tmp +
                     SEPARATEUR +
                     self.nom_destination.split('.')[0] +
                     SEPARATEUR)
        #
        # création liste des fichiers à concaténer
        #
        fichier = open(self.tmp + SEPARATEUR + "mylist.txt", "w")
        for i in range(len(self.liste_segments)):
            fichier.write('file partie_' + str(i) + '.mkv\n')
        fichier.close()
        #
        # concatenation des fichiers 
        #
        if len(self.liste_segments)>1 :
            macommand=FFMPEG_BIN + ' -safe 0 -f concat -i "' + \
                       self.tmp + SEPARATEUR + "mylist.txt" + \
                       '" -map v -c copy -map a -c copy -map s -c copy "' + \
                       self.tmp + SEPARATEUR + self.nom_destination.split('.')[0] +\
                       SEPARATEUR + self.nom_destination + '"'
            macommand = [FFMPEG_BIN, '-safe', '0', '-f', 'concat',  '-i',
                             self.tmp + SEPARATEUR + "mylist.txt", 
                             '-map', 'v?', '-c',  'copy',
                             '-map', 'a?', '-c', 'copy',
                             '-map', 's?', '-c', 'copy',
                             self.tmp + SEPARATEUR + self.nom_destination.split('.')[0] +
                             SEPARATEUR + self.nom_destination ]
            fichier = open(self.tmp + SEPARATEUR + "mycommande.txt", "a")
            
            fichier.write('concaténation finale  \n\n')
            fichier.write('\n commande : '  + str(' '.join(macommand)))
            fichier.write('\n\n\n')
            fichier.close()
            print(macommand)
            output = sp.Popen(macommand).communicate()[0]
            print("concaténation effectuée, fini")
        else:
            os.rename(self.tmp + SEPARATEUR + 'partie_0.mkv',
                      self.tmp + SEPARATEUR +
                      self.nom_destination.split('.')[0] + SEPARATEUR +
                      self.nom_destination)
            print("déplacement effectué, fini")

    def apropos(self):
        print('à propos')
        
    def aide(self):
        print('aide')
        pass

class Programmes(Toplevel):
    def __init__(self,projet = None):
        self.proj = projet
        self.GUI = Toplevel.__init__(self,projet.master)
        self.title('Programmes')
        self.geometry('+0+0')
        if projet.liste_programmes == []:
            messagebox.showinfo("Alerte", "veuillez choisir un fichier source")
        else :
            self.choix= LabelFrame(self, text = "choisir un  programme ")
            self.choix.grid(row=1,padx=5,pady=5)
            self.var_choix = IntVar()
            valeur = 0
            for prog in projet.liste_programmes:
                valeur += 1
                Radiobutton(self.choix, text=prog[1][2], variable=self.var_choix ,
                            value = valeur, command = self.select_programme).grid(row=valeur, column = 1)
            Button(self.choix,text='Ok', command=self.destroy).grid(row=valeur+1,column=1)
            
    def select_programme(self):
        self.proj.set_programme(self.proj.liste_programmes[self.var_choix.get()-1][0])

class Audio(Toplevel):
    def __init__(self,projet = None):
        self.proj = projet
        self.GUI = Toplevel.__init__(self,projet.master)
        self.title('Pistes audio')
        self.geometry('+300+0')
        if projet.liste_programmes == []:
            tkMessageBox.showinfo("Alerte", "veuillez choisir un programme")
        else :
            prog = projet.liste_programmes[projet.programme-1]
            liste_audio = [ prog[item][1] if prog[item][1][1] != None else [prog[item][1][0],'inconnu']
                            for item in range(2,len(prog)) if prog[item][0]== 'Audio']
            self.choix= LabelFrame(self, text = "choisir une ou des pistes audio ")
            self.choix.grid(padx=5,pady=5)
            self.var_choix = [IntVar() for i in liste_audio]
            valeur = 0
            for prog in liste_audio:
                valeur += 1
                Checkbutton(self.choix, text=prog[1], variable=self.var_choix[valeur-1],\
                            command = lambda a=liste_audio : self.select_audio(a),\
                            height=1,
                            width=20
                            ).grid(row=valeur, column = 1)
            Button(self.choix,text='Ok', command=self.destroy).grid(row=valeur+1,column=1)
     
                
    def select_audio(self,list_aud):
        self.proj.liste_audio=[]
        for i in range(0,len(self.var_choix)):
            if self.var_choix[i].get() == 1:
                self.proj.liste_audio.append(list_aud[i])
        self.proj.set_audio()

class Soustitre(Toplevel):
    def __init__(self,projet = None):
        self.proj = projet
        self.GUI = Toplevel.__init__(self,projet.master)
        self.title('Sous-titres')
        self.geometry('+600+0')
        if projet.liste_programmes == []:
            tkMessageBox.showinfo("Alerte", "veuillez choisir un programme")
        else :
            prog = projet.liste_programmes[projet.programme-1]
            liste_soustitre = [ prog[item][1] if prog[item][1][1] != None else [prog[item][1][0],'inconnu'] for item in range(2,len(prog)) if prog[item][0]== 'Sous-titre']
            self.choix= LabelFrame(self, text = "choisir une ou des pistes sous-titre ")
            self.choix.grid(padx=5,pady=5)
            self.var_choix = [IntVar() for i in liste_soustitre]
            valeur = 0
            for prog in liste_soustitre:
                valeur += 1
                Checkbutton(self.choix, text=prog[1], variable=self.var_choix[valeur-1],\
                            command = lambda a=liste_soustitre : self.select_soustitre(a),\
                            height=1,
                            width=20
                            ).grid(row=valeur, column = 1)
            Button(self.choix,text='Ok', command=self.destroy).grid(row=valeur+1,column=1)
                 
                
    def select_soustitre(self,list):
        self.proj.liste_soustitre=[]
        for i in range(0,len(self.var_choix)):
            if self.var_choix[i].get() == 1:
                self.proj.liste_soustitre.append(list[i])
        self.proj.set_soustitre()

class Rognage(Toplevel):
    def __init__(self,projet = None):
        self.GUI = Toplevel.__init__(self,projet.master)
        self.proj = projet
        self.title('Rognage')
        self.geometry('+0+100')
        self.reduction=720/int(self.proj.largeur_video)
        self.numero_image = 0
        if projet.source == ():
            messagebox.showinfo("Alerte", "veuillez choisir un fichier source")
        else :
            self.choix= LabelFrame(self, text = "choisir l'image puis  rogner")
            self.choix.grid(row=1,padx=5,pady=5)
            self.image_choix = Canvas(self.choix,width = 720, height = int(int(self.proj.hauteur_video)*self.reduction))
            self.image=self.image_choix.create_image(0,0, anchor = NW)
            self.image_choix.grid(row = 2,column =3)
            self.cadre = self.image_choix.create_rectangle(0,
                                                           0,
                                                           int(int(self.proj.largeur_video)*self.reduction),
                                                           int(int( self.proj.hauteur_video)*self.reduction),
                                                           outline='red')
            self.H_cadre=IntVar()
            self.H_cadre.set(0)
            self.G_cadre=IntVar()
            self.G_cadre.set(0)
            self.B_cadre=IntVar()
            self.B_cadre.set(self.proj.hauteur_video)
            self.D_cadre=IntVar()
            self.D_cadre.set(self.proj.largeur_video)
            self.regle_cadre()
            Button(self.choix,text='<-', command=lambda s =-1:self.select_image(s)).grid(row=5,column=2)
            Button(self.choix,text='->', command=lambda s =+1:self.select_image(s)).grid(row=5,column=4)
            Spinbox(self.choix, width = 5, increment = 2,
                    from_ =int(self.proj.largeur_video)/2,
                    to = int(self.proj.largeur_video),
                    textvariable=self.D_cadre,
                    command=self.regle_cadre).grid(row=3,column=5)
            Spinbox(self.choix, width = 5, increment = 2,
                    from_ =0, to = int(self.proj.largeur_video)/2,
                    textvariable=self.G_cadre,
                    command=self.regle_cadre).grid(row=3,column=1)
            Spinbox(self.choix, width = 5, increment = 2,
                    textvariable=self.H_cadre,
                    from_ =0, to = int(self.proj.hauteur_video)/2,
                    command=self.regle_cadre).grid(row=1,column=3)
            Spinbox(self.choix, width = 5, increment = 2,
                    from_ =int(self.proj.hauteur_video)/2,
                    to = int(self.proj.hauteur_video),
                    textvariable=self.B_cadre,
                    command=self.regle_cadre).grid(row=5,column=3)
            Button(self.choix,text='Ok', command=self.destroy).grid(row=4,column=3)
            self.select_image(+1)
    def affiche_image(self,ima):
        self.image_choix.itemconfig(self.image,image=ima)
        
    def select_image(self,num):
        self.numero_image += num
        print(self.proj.tmp + SEPARATEUR+'image'+str(self.numero_image).zfill(3)+'.PNG')
        if os.path.exists(self.proj.tmp + SEPARATEUR+'image'+str(self.numero_image).zfill(3)+'.PNG') :
            self.imag = Image.open(self.proj.tmp +SEPARATEUR+'image'+str(self.numero_image).zfill(3)+'.PNG')
            ## Remplace PhotoImage de Tkinter par celui de PIL
            self.imag0=self.imag.resize( (720, int(int(self.proj.hauteur_video)*self.reduction)), resample=0)
            self.phot=ImageTk.PhotoImage(self.imag0)
            self.affiche_image(self.phot)
        elif self.numero_image == 0 :
            self.numero_image = 1
        else :
            self.numero_image -= num
 
    def regle_cadre(self):
        self.image_choix.delete(self.cadre)
        self.cadre=self.image_choix.create_rectangle(int(self.G_cadre.get()*self.reduction),
                                          int(self.H_cadre.get()*self.reduction),
                                          int(self.D_cadre.get()*self.reduction),
                                          int(self.B_cadre.get()*self.reduction),
                                          outline='red')
        self.proj.set_rognage('x'.join([str(self.G_cadre.get()),str(self.H_cadre.get()),
                               str(self.D_cadre.get()), str(self.B_cadre.get())]))

class Decoupage(Toplevel):
    def __init__(self,projet = None):
        self.proj = projet
        self.GUI = Toplevel.__init__(self,projet.master)
        self.title('Decoupage')
        Button(self, text='play', command=self.play).grid(row=1, column=1)
        Label(self, text='q pour quitter \n s pour pas à pas').grid(row=2, column=1)
        Label(self, text='Position film : ').grid(row=3, column = 1)
        self.pos=StringVar()
        self.pos.set('')
        self.eti=Entry(self, textvariable=self.pos)
        self.eti.grid(row=4,column=1)
        self.choix= LabelFrame(self, text = "Liste des segments à incorporer")
        self.choix.grid(row=5, column=1,padx=5,pady=5)
        self.decoupage=[]
        self.decoupage = [Segment_Video(self)]
        if len(self.decoupage) == 1:
            self.decoupage[0].debut.set('00:00:00.000')
        if projet.source == ():
            messagebox.showinfo("Alerte", "veuillez choisir un fichier source")
        Button(self,text='Ok', command=self.valide_decoupage).grid(row=6,column=1)
        
    def cree_segment(self):
        self.decoupage.append(Segment_Video(self))
        self.set_decoupage()
        
    def supprime_segment(self):
        if len(self.decoupage)>1:
            self.decoupage.pop().destroy()
        self.set_decoupage()
            
    def play(self):
        threading.Thread(target=self.player).start()
        
    def player(self):
        #filtergraph="drawtext=fontsize=80:text=\'%{pts\\:s}\':box=1:x=(w-tw)/2:y=h-(2*lh)"
        if self.proj.nom_source.split('.')[1] == 'ts':
            command = [ FFPLAY_BIN,'-i', self.proj.source,
                        '-genpts',
                        '-vst','p:'+self.proj.liste_programmes[self.proj.programme-1][1][1],
                        '-x','600','-y','400'#,
                        #'-vf',filtergraph
                        ]
        else:
            command = [ FFPLAY_BIN,'-i', self.proj.source,
                        '-genpts',
                        '-x','600','-y','400'#,
                        #'-vf',filtergraph
                        ]
        proc=sp.Popen(command, bufsize=1, stdout=sp.PIPE, stderr=sp.STDOUT,  universal_newlines=True)
        texte=""
        temps_initial= 0 # float(self.proj.start)
        texto = proc.stdout.readline(1)
        while texto != '' or proc.poll() is None:
            if '\r' in texto or '\n' in texto :
                if 'A-V:' in texte:
                    txt = float(texte.split(' A-V:')[0])
                    if temps_initial > 0:
                        self.pos.set('%10.3f' %(txt-temps_initial))
                    else:
                        temps_initial = txt
                        print(temps_initial)
                texte=""
            else:
                texte = texte + texto
            texto = proc.stdout.readline(1)
        print('fini')

    def set_decoupage(self):
        self.proj.liste_segments=[(seg.debut.get(), seg.fin.get()) for seg in self.decoupage]
        self.proj.set_decoupage(self.proj.liste_segments)
    def valide_decoupage(self):
        self.set_decoupage()
        self.destroy()
    
class Segment_Video(Frame):
    def __init__(self,root):
        Frame.__init__(self,root.choix)
        self.root=root
        Button(self,text='+', command=root.cree_segment).grid(row=1,column=1)
        Button(self,text='-', command=root.supprime_segment).grid(row=1,column=2)
        self.debut_label = Label(self,text = ' De :', width = 13)
        self.debut_label.grid(row=1, column=3)
        self.debut = StringVar()
        Entry(self, textvariable= self.debut, width = 13).grid(row=1, column=4)
        Button(self,text='<-ici', command=self.set_point_debut).grid(row=1,column=5)
        self.fin_label = Label(self,text = ' À :', width = 13)
        self.fin_label.grid(row=1, column=6)
        self.fin = StringVar()
        Entry(self, textvariable= self.fin, width = 13).grid(row=1, column=7)
        Button(self,text='<-ici', command=self.set_point_fin).grid(row=1,column=8)
        self.grid()
        
    def set_point_debut(self):
        self.debut.set(self.root.pos.get())
        
    def set_point_fin(self):
        self.fin.set(self.root.pos.get())
     
#définition des variables globales
if sys.platform.startswith('win'):
    FFPROBE_BIN="ffprobe.exe"# sortie de ffprobe vers stderr
    FFPLAY_BIN="ffplay.exe"#
    FFMPEG_BIN="ffmpeg.exe"#
    SEPARATEUR="/"
    HOME = "C:\\Users\\quenton\\Documents\\Python Scripts\\monvideomixeur"
elif sys.platform.startswith('linux'):
    FFPROBE_BIN="ffprobe"# sortie de ffprobe vers stderr
    FFPLAY_BIN="ffplay"#
    FFMPEG_BIN="ffmpeg"#
    SEPARATEUR="/"
    HOME = "~"
if __name__=="__main__":
    Mon_projet = MonApplication(60,30)    
    Mon_projet.mainloop()
    
