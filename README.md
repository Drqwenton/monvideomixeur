Utilisation du video mixeur

Sélectionner un fichier ts (à l'origine le programme est prévu d'avoir en entrée un fichier enregistrée avec un tuner format ts)

Sélectionner un "fichier destination", en fait le programme va construire un dossier tmp_nom_de_la_destination (si non précisé, le dossier est crée sur C:\)

Sélectionner le programme (si un seul dans le fichier, il est écrit unique)

Sélectionner les langues audio et sous titre

ensuite : le découpage permet de chercher les tranches de film à conserver. (problème sous windows l'affichage du temps se fait par saut)

le rognage crée cinq capture d'écran à partir du temps du premier segment de découpage, ou à partir de 0.0 s
 permet de ne conserver que l'image sans les bandes noire
 
Pour lancer la conversion : c'est la quatrième icone (juste avant la bouée)

La conversion crée un ou plusieurs fichiers dans le dossier tmp_..., et en fin de travail, crée à l'intérieur un dossier portant le nom 
du fichier et la concaténation des fichiers temporaires.


(beaucoup d'icones et menus sont sans actions)