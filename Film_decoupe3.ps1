[System.Console]::OutputEncoding = [System.Text.Encoding]::UTF8
$nom_fichier=(Get-ChildItem *.ts,*.m2ts,*.mkv).name.split(".")[0]
$chemin="C:\Users\quenton\Videos\ffmpeg-20171204-71421f3-win64-static\bin\"
$type_fichier=(Get-ChildItem *.ts,*.m2ts,*.mkv).name.split(".")[1]
$rognage=' -filter:v "crop=1920:1080:0:0" '
$video=" -map 0:0 -vcodec libx264 "
$audio=" -map 0:1 -map 0:2  -acodec eac3 "#
$soustitre=" -map 0:4 -scodec copy " 

$debut_1="00:13:00.35"
#$debut_1="00:01:18.00"
$fin_1="00:44:46.55"
#$fin_1="01:10:01.50"
$duree_1= New-TimeSpan -start $debut_1 -end $fin_1

$debut_2="00:50:41.562"
#$debut_2="00:02:25.00"
$fin_2="01:34:31.65"
#$fin_2="00:02:45.0"
$duree_2= New-TimeSpan -start $debut_2 -end $fin_2

#$debut_3="00:03:14.00"
$debut_3="01:40:05.119"
#$fin_3="00:03:34.0"
$fin_3="02:03:51.0"
$duree_3= New-TimeSpan -start $debut_3 -end $fin_3


$macommand= '.\ffmpeg.exe -y -max_error_rate 1 -v verbose -max_interleave_delta 2000000 -i ' +'"'+$chemin+$nom_fichier+"."+$type_fichier+'" -threads 4 -preset slow -crf 21 -tune film -profile:v high10 -ss '+$debut_1+$rognage+' -t '+$duree_1+$video +$audio+$soustitre+  ' ".\partie 01.mkv"'+' -ss '+$debut_2 +$rognage+' -t '+$duree_2+$video +$audio+$soustitre+  ' ".\partie 02.mkv"'+' -ss '+$debut_3 +$rognage+' -t '+$duree_3+$video +$audio+$soustitre+  ' ".\partie 03.mkv"'
#$macommand= '.\ffmpeg.exe -y -max_error_rate 1 -v verbose -max_interleave_delta 2000000 -i ' +'"'+$chemin+$nom_fichier+"."+$type_fichier+'" -threads 4 -preset slow -crf 21 -tune film -profile:v high10 -ss '+$debut_1+$rognage+' -t '+$duree_1+$video +$audio+$soustitre+  ' ".\partie 01.mkv"'

Invoke-Expression $macommand

$macommand='mkdir ".\'+$nom_fichier+'"'
Invoke-Expression $macommand

$macommand="echo ('file ''.\partie 01.mkv''','file ''.\partie 02.mkv''', 'file ''.\partie 03.mkv''') | Out-File -Encoding Default mylist.txt"
Invoke-Expression $macommand


$command_concat='.\ffmpeg -safe 0 -f concat -i mylist.txt -map v -c copy -map a -c copy -map s -c copy ".\'+$nom_fichier+'\'+$nom_fichier+'.mkv"'
Invoke-Expression $command_concat


#~ shutdown.exe /p

#~ pause

#~ ./ffmpeg -i '.\Impitoyable (VM).ts' -copy_unknown -scodec copy -y  -filter_complex \
#"\
#[0:26] trim=start='00\:00\:10\.0':end='00\:00\:20\.0',setpts=PTS-STARTPTS [a1];\
#[0:26] trim=start='00\:01\:25\.0':end='00\:01\:40\.0',setpts=PTS-STARTPTS [a2];\
#[0:34] atrim=start='00\:00\:10\.0':end='00\:00\:20\.0',asetpts=PTS-STARTPTS [b1];\
#[0:34] atrim=start='00\:01\:25\.0':end='00\:01\:40\.0',asetpts=PTS-STARTPTS [b2];\
#[0:33] atrim=start='00\:00\:10\.0':end='00\:00\:20\.0',asetpts=PTS-STARTPTS [c1];\
#[0:33] atrim=start='00\:01\:25\.0':end='00\:01\:40\.0',asetpts=PTS-STARTPTS [c2];\
#[a1][a2] concat [at];\
#[b1][b2] concat [bt];\
#[c1][c2] concat [ct];\
#" -map [at] -map [bt] -map [ct] testf.mkv
